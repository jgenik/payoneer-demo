package com.naya.payoneer.kuduTest

import io.circe.generic.auto._
import io.circe.parser._

class DataConverter extends Serializable {

    def convert(data:String): ReceivedData ={

        //data conversion and enrichment logic here
        val converted = decode[ReceivedData](data)

        converted.right.get
    }

}
