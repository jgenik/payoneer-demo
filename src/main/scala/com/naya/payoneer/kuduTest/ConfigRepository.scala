package com.naya.payoneer.kuduTest

class ConfigRepository extends Serializable {

    val configurations = Map(
        "master" -> "local[2]",
        "app.name" -> "PayoneerApp",
        "kafka.url" -> "localhost:9092",
        "consume.topic" -> "test2",
        "produce.topic" -> "produceTopic1",
        "kudu.store.table" -> "payoneer_table",
        "kudu.url" -> "localhost:7051"
    )

    def getProperty(name : String) :String = {
        configurations(name)
    }

}
