package com.naya.payoneer.kuduTest


import java.util
import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kudu.ColumnSchema.ColumnSchemaBuilder
import org.apache.kudu.{Schema, Type}
import org.apache.kudu.client._
import org.apache.spark.sql.{ForeachWriter, Row, SparkSession}
import org.apache.spark.sql.streaming.OutputMode

import scala.collection.JavaConversions

object KafkaStreamer {

    def main(args: Array[String]): Unit = {

        val dataConverter: DataConverter = new DataConverter

        val configRepository = new ConfigRepository

        val foreachWriter: PayoneerForeachWriter = new PayoneerForeachWriter

        val spark = SparkSession
          .builder
          .appName(configRepository.getProperty("app.name"))
          .master(configRepository.getProperty("master"))
          .getOrCreate()


        val df = spark
          .readStream
          .format("kafka")
          .option("kafka.bootstrap.servers", configRepository.getProperty("kafka.url"))
          .option("subscribe", configRepository.getProperty("consume.topic"))
          .load()


        import spark.implicits._
        val frame = df.selectExpr("CAST(value AS STRING)")

        val query = frame.writeStream.foreach(new PayoneerForeachWriter).start()

        query.awaitTermination()
    }

}




/*
*
* new ForeachWriter[Row] {

            var kuduClient: KuduClient = null
            var kuduSession: KuduSession = null
            var targetTable: KuduTable = null

            val kafkaProperties = new Properties()
            kafkaProperties.put("bootstrap.servers", configRepository.getProperty("kafka.url"))
            kafkaProperties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
            kafkaProperties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
            val results = new scala.collection.mutable.HashMap[String, String]
            var producer: KafkaProducer[String, String] = _

            def open(partitionId: Long, version: Long): Boolean = {
                // open connection
                kuduClient = new KuduClient.KuduClientBuilder(configRepository.getProperty("kudu.url")).build()
                kuduSession = kuduClient.newSession()
                targetTable = kuduClient.openTable(configRepository.getProperty("kudu.store.table"))

                producer = new KafkaProducer(kafkaProperties)
                true
            }

            def process(record: Row) = {
                val convertedRecord = dataConverter.convert(record.mkString)
                println(convertedRecord.id)

                val insert = targetTable.newInsert
                val row = insert.getRow
                row.addLong(0, convertedRecord.id)
                row.addString(1, convertedRecord.name)
                kuduSession.apply(insert)

                producer.send(new ProducerRecord(configRepository.getProperty("produce.topic"), convertedRecord.toString))
            }

            def close(errorOrNull: Throwable): Unit = {
                // close the connection
                kuduClient.close()
            }
        }
*
*
* */
