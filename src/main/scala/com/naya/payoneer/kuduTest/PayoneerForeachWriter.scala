package com.naya.payoneer.kuduTest

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kudu.client.{KuduClient, KuduSession, KuduTable}
import org.apache.spark.sql.{ForeachWriter, Row}

class PayoneerForeachWriter extends ForeachWriter[Row]{

    var configRepository = new ConfigRepository
    var dataConverter: DataConverter = new DataConverter

    var kuduClient: KuduClient = null
    var kuduSession: KuduSession = null
    var targetTable: KuduTable = null

    val kafkaProperties = new Properties()
    kafkaProperties.put("bootstrap.servers", configRepository.getProperty("kafka.url"))
    kafkaProperties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    kafkaProperties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    val results = new scala.collection.mutable.HashMap[String, String]
    var producer: KafkaProducer[String, String] = _

    override def open(partitionId: Long, version: Long): Boolean = {
        kuduClient = new KuduClient.KuduClientBuilder(configRepository.getProperty("kudu.url")).build()
        kuduSession = kuduClient.newSession()
        targetTable =  kuduClient.openTable(configRepository.getProperty("kudu.store.table"))

        producer = new KafkaProducer(kafkaProperties)
        true
    }

    override def process(value: Row): Unit = {
        val convertedRecord = dataConverter.convert(value.mkString)
        println(convertedRecord.id)

        val insert = targetTable.newInsert
        val row = insert.getRow
        row.addLong(0, convertedRecord.id)
        row.addString(1, convertedRecord.name)
        kuduSession.apply(insert)

        producer.send(new ProducerRecord(configRepository.getProperty("produce.topic"), convertedRecord.toString))
    }

    override def close(errorOrNull: Throwable): Unit = {
        kuduClient.close()
    }
}
