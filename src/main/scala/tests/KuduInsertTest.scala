package tests

import org.apache.kudu.client.KuduClient

object KuduInsertTest {

    def main(args: Array[String]): Unit = {
        println("Started----")

        val kuduClient = new KuduClient.KuduClientBuilder("localhost:7051").build()

        val kuduSession = kuduClient.newSession()

        val targetTable = kuduClient.openTable("test_table")

        val rows = Range(0, 3).map { i =>
            val insert = targetTable.newInsert
            val row = insert.getRow
            row.addInt(0, i)
            row.addInt(1, i)

            kuduSession.apply(insert)
        }

        kuduClient.close()

        println("Finished----")
    }

}
