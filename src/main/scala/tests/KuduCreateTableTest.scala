package tests

import java.util

import org.apache.kudu.ColumnSchema.ColumnSchemaBuilder
import org.apache.kudu.client.{CreateTableOptions, KuduClient}
import org.apache.kudu.{Schema, Type}

import scala.collection.JavaConversions

object KuduCreateTableTest {
    def main(args: Array[String]): Unit = {
        println("Started-----")

        val kuduClient = new KuduClient.KuduClientBuilder("localhost:7051").build()

       /* val schemas = List(
            new ColumnSchema.ColumnSchemaBuilder("personName",Type.STRING).build()
            , new ColumnSchema.ColumnSchemaBuilder("personAge",Type.STRING).build())
          .asInstanceOf[util.ArrayList[ColumnSchema]]*/


      /*  val columnsList:util.ArrayList[ColumnSchema] = new util.ArrayList[ColumnSchema]()
        columnsList.add(new ColumnSchemaBuilder("key", Type.INT32).key(true).build())

        print(columnsList)


         new ColumnSchemaBuilder("c2_s", Type.STRING).nullable(true).build(),
                new ColumnSchemaBuilder("c3_double", Type.DOUBLE).build(),
                new ColumnSchemaBuilder("c4_long", Type.INT64).build(),
                new ColumnSchemaBuilder("c5_bool", Type.BOOL).build(),
                new ColumnSchemaBuilder("c6_short", Type.INT16).build(),
                new ColumnSchemaBuilder("c7_float", Type.FLOAT).build(),
                new ColumnSchemaBuilder("c8_binary", Type.BINARY).build(),
                new ColumnSchemaBuilder("c9_unixtime_micros", Type.UNIXTIME_MICROS).build(),
                new ColumnSchemaBuilder("c10_byte", Type.INT8).build(),
                new ColumnSchemaBuilder("c11_decimal32", Type.DECIMAL)
                  .typeAttributes(
                      new ColumnTypeAttributesBuilder().precision(DecimalUtil.MAX_DECIMAL32_PRECISION).build()
                  ).build(),
                new ColumnSchemaBuilder("c12_decimal64", Type.DECIMAL)
                  .typeAttributes(
                      new ColumnTypeAttributesBuilder().precision(DecimalUtil.MAX_DECIMAL64_PRECISION).build()
                  ).build(),
                new ColumnSchemaBuilder("c13_decimal128", Type.DECIMAL)
                  .typeAttributes(
                      new ColumnTypeAttributesBuilder().precision(DecimalUtil.MAX_DECIMAL128_PRECISION).build()
                  ).build())



        */

       // val schema = new Schema(schemas)







        val schema: Schema = {
            val columns = List(
                new ColumnSchemaBuilder("key", Type.INT64).key(true).build(),
                new ColumnSchemaBuilder("name", Type.STRING).build())

            new Schema(JavaConversions.seqAsJavaList(columns))
        }

        val rangePartition:util.ArrayList[String] = new util.ArrayList[String]()
        rangePartition.add("key")
        //rangePartition.add("c1_i")

        kuduClient.createTable("payoneer_table",schema,
            new CreateTableOptions().setRangePartitionColumns(rangePartition).setNumReplicas(1))
        // Use KuduContext to create, delete, or write to Kudu tables
        //val kuduContext = new KuduContext("kudu.master:7051", sqlContext.sparkContext)

        kuduClient.close()

        println("Finished----")
    }
}
