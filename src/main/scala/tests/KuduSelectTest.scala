package tests

import org.apache.kudu.client.KuduClient

object KuduSelectTest {

    def main(args: Array[String]): Unit = {
        println("Started----")

        val kuduClient = new KuduClient.KuduClientBuilder("127.0.0.1:7050").build()

        val targetTable = kuduClient.openTable("test_table")



        val scanner = kuduClient.newScannerBuilder(targetTable).limit(1)
          .build()

        while (scanner.hasMoreRows){
            println (scanner.nextRows().next().toString)
        }

        println("Finished----")
    }

}
